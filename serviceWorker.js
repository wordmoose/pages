const cacheName = "wordmoose-v20230905-2";
const cachingStrategy = 'cache-first'; // network-first or cache-first
const assets = [
  "/",
  "/index.html",
  "/css",
  "/css/style.css",
  "/js",
  "/js/app.js",
  "/js/game.js",
  "/js/menu.js",
  "/js/keyEventRegistration.js",
  "/js/serviceWorkerRegistration.js",
  "/js/settings.js",
  "/js/wordlists/wordlistEn.js",
  "/js/wordlists/wordlistNo.js",
  "/images",
  "/images/8a1e553f-1abf-4df1-b6a1-e0d0b75cad78.jpeg",
];


self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(cacheName).then(cache => {
      try {
        cache.addAll(assets)
      } catch (error) {
        console.error(error);
      }
    }).then(() => {
      caches.keys().then((cacheNames) => {
        for (let cacheName of cacheNames.sort().slice(0, -1)) {
          caches.delete(cacheName);
        }
      })
    })
  )
});


switch (cachingStrategy) {
  case 'network-first':
    self.addEventListener("fetch", fetchEvent => {
      fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(res => {
          return res || fetch(fetchEvent.request)
        })
      )
    });
    break;
  
  case 'cache-first':
    self.addEventListener("fetch", fetchEvent => {
      fetchEvent.respondWith(
        fetch(fetchEvent.request).catch(
          () => caches.match(fetchEvent.request)
        )
      )
    });
    break;
}
