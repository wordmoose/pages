'use strict'


function handleKeyEvent(event) {
  event = event || window.event;
  console.log('Key event registered:', event);

  if (event.key === ' ') {
    currentGame = Game.fromRandom();
  }

  if (currentGame === undefined) {
    return
  }

  if (currentGame.getStatus() != 'ongoing') {
    return
  }

  if (event.code.slice(0, 3) === 'Key') {
    currentGame.addLetter(event.key);
  }
  else if (['æ', 'ø', 'å'].includes(event.key)) {
    currentGame.addLetter(event.key);
  }
  else if (event.key === '-') {
    currentGame.removeLetter();
  }
  else if (event.key === 'Enter') {
    currentGame.submitGuess();
  }
}

document.addEventListener('keypress', event => this.handleKeyEvent(event));
