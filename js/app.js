'use strict';


let currentGame;

let newGameButton = document.querySelector('#new-game-button');
newGameButton.onclick = () => {
  currentGame = Game.fromRandom();
};


let menuButton = document.querySelector('#menu-button');
menuButton.onclick = () => {
  showMenu();
}


if (localStorage.getItem('toOpen')) {
  switch (localStorage.getItem('toOpen')) {
    case 'menu':
      showMenu();
      break;
  }
  localStorage.setItem('toOpen', '');
}


async function requestPersistentStorage() {
  if (navigator.storage && navigator.storage.persist) {
    const isPersisted = await navigator.storage.persist();
    console.log(`Persisted storage granted: ${isPersisted}`);
  }
}
