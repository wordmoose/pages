'use strict';


class Game {
  constructor(
    language,
    nAllowedGuesses,
    percentileWordLengthFilterGuess,
    word,
    settings
  ) {
    this.language = language;
    this.nAllowedGuesses = nAllowedGuesses;
    this.percentileWordLengthFilterGuess = percentileWordLengthFilterGuess;
    this.word = word;
    this.settings = settings;

    this.startTime = new Date().toISOString();

    this.guesses = [];
    this.status = 'ongoing';

    this.displayGUI();
    this.saveToLocalStorage();
  }


  static fromRandom() {
    
    let settings = getSettings();

    let language = settings['language'];
    let wordLength = Number(settings['wordLength']);
    let percentileWordLengthFilter = Number(settings['percentileWordLengthFilter']);
    let percentileWordLengthFilterGuess = Number(settings['percentileWordLengthFilterGuess']);
    let nAllowedGuesses = Number(settings['nAllowedGuesses']);

    let word = Game.getRandomWord(language, wordLength, percentileWordLengthFilter);
    let game = new this(
      language,
      nAllowedGuesses,
      percentileWordLengthFilterGuess,
      word,
      settings
    )
    return game
  }


  static getWordList(language, wordLengthFilter, percentileWordLengthFilter) {
    const wordListText = {
      'en': wordListTextEnglish,
      'no': wordListTextNorwegian
    }[language]
    
    let wordListWordLength = wordListText.split('\n').filter(word => word.length === wordLengthFilter);
    let nTopWordsLimit = Math.floor((percentileWordLengthFilter/100) * wordListWordLength.length);
    return wordListWordLength.slice(0, nTopWordsLimit)
  }
  
  
  static getRandomWord(language, wordLength, percentileWordLengthFilter) {
    let randomWordSelectionPool = Game.getWordList(language, wordLength, percentileWordLengthFilter);
    return randomWordSelectionPool[Math.floor(Math.random() * (randomWordSelectionPool.length + 1))]
  }
  

  saveToLocalStorage() {
    requestPersistentStorage();

    if (!localStorage.getItem('games')) {
      localStorage.setItem("games", JSON.stringify({}));
    }

    let games = JSON.parse(localStorage.getItem('games'));
    games[this.startTime] = this;
    localStorage.setItem('games', JSON.stringify(games));
  }


  addLetter(letter) {
    let guess = this.boardDiv.children[this.guesses.length];
    if (guess.textContent.length >= this.word.length) {
      return
    }
    [...guess.children].filter(span => span.textContent === '')[0].innerText = letter;
  }


  removeLetter() {
    let guess = this.boardDiv.children[this.guesses.length];
    if (guess.textContent.length === 0) {
      return
    }
    [...guess.children].filter(span => span.textContent !== '').at(-1).innerText = '';
  }


  getLetterColor(letterPos, guessWord) {
    let letter = guessWord[letterPos];

    if (this.word[letterPos] === letter) {
      return 'correct'
    }

    if (this.word.includes(letter)) {
      let nCorrectLetterGuesses = guessWord.split('').map((l, i) => l === this.word[i] & l === letter).reduce((partialSum, a) => partialSum + a, 0);
      let nAlreadyMisplacedLetterGuesses = guessWord.split('').map((l, i) => 
        this.word.includes(l) &
        l !== this.word[i] &
        l === letter &
        i < letterPos
      ).reduce((partialSum, a) => partialSum + a, 0) || 0;
      
      if ((this.word.split(letter).length - 1) > (nCorrectLetterGuesses + nAlreadyMisplacedLetterGuesses)) {
        return 'misplaced'
      }
    }

    return 'incorrect'
  }


  submitGuess() {
    this.showMessage('');  // Clear any former message

    let guess = this.boardDiv.children[this.guesses.length];
    let guessWord = guess.textContent;

    if (guessWord.length < this.word.length) {
      this.showMessage('💬 Word too short');
      return
    }

    let allowedGuessWordList = Game.getWordList(this.language, this.word.length, this.percentileWordLengthFilterGuess);

    if (!allowedGuessWordList.includes(guessWord)) {
      this.showMessage('❓ Unknown word');
      return
    }

    for (let letterGuessPosition of [...this.word.split('').keys()]) {
      
      let guessLetterSpan = guess.children[letterGuessPosition];
      let guessLetterColor = this.getLetterColor(letterGuessPosition, guessWord)
      let keyboardLetterSpan = document.querySelector(`#letter-${guessWord[letterGuessPosition]}`);

      guessLetterSpan.classList.add(guessLetterColor);
      keyboardLetterSpan.classList.add(guessLetterColor);
    }

    this.guesses.push(guessWord);
    this.status = this.getStatus();
    this.saveToLocalStorage();

    switch (this.status) {
      case 'ongoing':
        return  // Don't end game
      case 'won':
        this.showMessage(`🏆 Correct word - ${this.word.toUpperCase()}! 🎉`);
        break;
      case 'lost':
        this.showMessage(`❌ Game over - word was ${this.word.toUpperCase()}.`);
        break;
    }

    this.end();
  }

      
  displayGUI() {
    this.boardDiv = document.createElement('div');
    this.boardDiv.id = 'board';

    for (let _ of [...Array(this.nAllowedGuesses)]) {
      let guessP = document.createElement('p');
      guessP.classList.add('guess', 'letter-row');

      for (let _ of this.word) {
        let guessLetterSpan = document.createElement('span');
        guessLetterSpan.classList.add('guess-letter', 'letter', 'upper');
        guessP.appendChild(guessLetterSpan);
      }
      this.boardDiv.appendChild(guessP);
    }

    let messageDiv = document.createElement('div');
    messageDiv.id = 'message';
    this.messageP = document.createElement('p');
    this.messageP.innerText = '';
    messageDiv.appendChild(this.messageP);

    let keyboardDiv = document.createElement('div');
    keyboardDiv.id = 'keyboard';

    let keyboardRows = {'en': [
      'qwertyuiop',
      'asdfghjkl',
      '⇐zxcvbnm⏎'
    ],
    'no': [
      'qwertyuiopå',
      'asdfghjkløæ',
      '⇐zxcvbnm⏎'
    ]}[this.language];

    for (let keyboardRow of keyboardRows) {
      let keyboardRowDiv = document.createElement('div');
      keyboardRowDiv.classList.add('keyboard-row', 'letter-row');

      for (let letter of keyboardRow.split('')) {
        let keyboardLetterSpan = document.createElement('span');
        keyboardLetterSpan.id = `letter-${letter}`;
        keyboardLetterSpan.classList.add('keyboard-letter', 'letter', 'upper', 'clickable');
        keyboardLetterSpan.innerText = letter;
        keyboardLetterSpan.onclick = () => this.inputLetter(letter);

        keyboardRowDiv.appendChild(keyboardLetterSpan);
      }
      keyboardDiv.appendChild(keyboardRowDiv);
    }

    let main = document.querySelector('main');
    main.innerHTML = '';

    main.appendChild(this.boardDiv);
    main.appendChild(messageDiv);
    main.appendChild(keyboardDiv);
  }


  getStatus() {
    if ([...this.boardDiv.children].slice(0, this.guesses.length).some((guess) => guess.textContent === this.word)) {
      return 'won'
    }
    if (this.guesses.length < this.nAllowedGuesses) {
      return 'ongoing'
    }
    return 'lost'
  }


  inputLetter(letter) {
    if (this.getStatus() != 'ongoing') {
      return
    }
    
    switch (letter) {
      case '⏎':
        this.submitGuess();
        break;
      case '⇐':
        this.removeLetter();
        break;
      default:
        this.addLetter(letter);
    }
  }


  showMessage(message) {
    this.messageP.innerText = message;
  }


  end() {
    this.endTime = new Date().toISOString();
    this.saveToLocalStorage();
    showStats(true);
  }
}
