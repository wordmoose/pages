'use strict'


/* Menus */

function showMenu() {

  let menuDialog = document.querySelector('#menu-dialog');

  let openHowToPlayLink = document.querySelector('#open-how-to-play-link');
  openHowToPlayLink.onclick = () => {
    menuDialog.close();
    showHowToPlay();
  };

  let openSettingsLink = document.querySelector('#open-settings-link');
  openSettingsLink.onclick = () => {
    menuDialog.close();
    showSettings();
  };

  let openStatisticsLink = document.querySelector('#open-statistics-link');
  openStatisticsLink.onclick = () => {
    menuDialog.close();
    showStats();
  };

  let openHistoryLink = document.querySelector('#open-history-link');
  openHistoryLink.onclick = () => {
    menuDialog.close();
    showHistory();
  };

  let openAboutLink = document.querySelector('#open-about-link');
  openAboutLink.onclick = () => {
    menuDialog.close();
    showAbout();
  };

  caches.keys().then((cacheNames) => {
    const versionText = document.querySelector('#version-text');
    versionText.innerText = (cacheNames.length === 1) ? cacheNames[0].replace('wordmoose-', '') : 'check cache';
  });

  menuDialog.showModal();
  menuDialog.scrollTo(0, 0);
}


function showHowToPlay() {
  let howToDialog = document.querySelector('#how-to-play-dialog');
  registerBackButton(howToDialog);
  howToDialog.showModal();
  howToDialog.scrollTo(0, 0);
}


function showSettings() {
  let settings = getSettings();

  let languageSelect = document.querySelector("#language-select");
  languageSelect.value = settings['language'];
  
  let wordLengthSelect = document.querySelector("#word-length-select");
  wordLengthSelect.value = settings['wordLength'];

  let wordDifficultySelect = document.querySelector("#word-difficulty-select");
  wordDifficultySelect.value = settings['wordDifficulty'];

  let saveSettingsButton = document.querySelector('#save-settings-button');
  saveSettingsButton.onclick = () => {
    saveSettings();
  };

  let settingsDialog = document.querySelector('#settings-dialog');
  registerBackButton(settingsDialog);
  settingsDialog.showModal();
  settingsDialog.scrollTo(0, 0);
}


function showStats(showNewGameButton=false) {
  
  let resetStatisticsButton = document.querySelector('#reset-stats-button');
  resetStatisticsButton.onclick = () => {
    if (confirm('Do you really want to reset stats?')) {
      resetStats();
    }
  };

  let statsDiv = document.querySelector('#stats-div');
  statsDiv.innerHTML = '';
  let statsDivs = getStatsDivs();
  statsDivs.forEach((div) => statsDiv.appendChild(div));

  let statsDialog = document.querySelector('#stats-dialog');
  registerBackButton(statsDialog);

  if (showNewGameButton) {
    let statsButtonRow = document.querySelector('#stats-button-row');
    let newGameButton = document.createElement('button');
    newGameButton.innerText = 'New game';
    newGameButton.classList.add('button', 'clickable');
    newGameButton.onclick = () => {
      newGameButton.remove();
      statsDialog.close();
      currentGame = Game.fromRandom();
    }
    statsButtonRow.appendChild(newGameButton);

    // Clean up new game button on close
    let closeButton = statsDialog.querySelector('.dialog-confirm');
    closeButton.onclick = () => {
      newGameButton.remove();
    }

    // Clean up new game button on back
    let backLink = statsDialog.querySelector('.back-link');
    let backLinkOnclick = backLink.onclick;
    backLink.onclick = () => {
      newGameButton.remove();
      backLinkOnclick();
    }
  }

  statsDialog.showModal();
  statsDialog.scrollTo(0, 0);
}


function showHistory() {
  let resetHistoryButton = document.querySelector('#reset-history-button');
  resetHistoryButton.onclick = () => {
    if (confirm('Do you really want to reset game history?')) {
      resetStats();
    }
  };

  let historyDiv = document.querySelector('#history-div');
  historyDiv.innerHTML = '';

  let games = getGames();
  let gameDivs = games.sort((game) => game.startTime).reverse().map(gameToDiv);
  gameDivs.forEach((gameDiv) => historyDiv.appendChild(gameDiv));

  let historyDialog = document.querySelector('#history-dialog');
  registerBackButton(historyDialog);
  historyDialog.showModal();
  historyDialog.scrollTo(0, 0);
}


function showAbout() {
  let aboutDialog = document.querySelector('#about-dialog');
  registerBackButton(aboutDialog);
  aboutDialog.showModal();
  aboutDialog.scrollTo(0, 0);
}


/* Stats */

function getGames() {
  requestPersistentStorage();

  if (!localStorage.getItem('games')) {
    localStorage.setItem("games", JSON.stringify({}));
  }

  return Object.values(JSON.parse(localStorage.getItem('games')));
}


function resetStats() {
  let statsDialog = document.querySelector('#stats-dialog');

  requestPersistentStorage();
  localStorage.setItem("games", JSON.stringify({}));
  statsDialog.close();
  showStats();
}


function getStatsDivs() {

  // Calculations

  let allGames = getGames();
  let currentSettings = getSettings();
  let currentSettingsText = settingsToText(currentSettings);
  let games = allGames.filter((game) => settingsToText(game.settings) === currentSettingsText);

  let completedGames = games.filter(game => game.status !== 'ongoing');
  let wonGames = games.filter(game => game.status === 'won');
  let allGuessCounts = [...Array(currentSettings['nAllowedGuesses']).keys()].reduce((a, c) => (a[c+1]=0, a), {});
  let wonGamesByGuessCount = wonGames.map(game => game.guesses.length).reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), allGuessCounts);

  let longestStreak = 0;
  let longestStreakTemp = 0;
  for (let game of completedGames) {
    if (game.status === 'won') {
      longestStreakTemp += 1;
    } else {
      longestStreak = (longestStreakTemp > longestStreak) ? longestStreakTemp : longestStreak;
      longestStreakTemp = 0;
    }
  }
  longestStreak = (longestStreakTemp > longestStreak) ? longestStreakTemp : longestStreak;

  // Header div

  let statsHeaderDiv = document.createElement('div');
  statsHeaderDiv.innerText = currentSettingsText;

  let gameSummaryDiv = document.createElement('div');
  gameSummaryDiv.innerText = `🔥   Current streak: ${completedGames.sort(game => game.startTime).map(game => game.status === 'won').reduce((a, c) => (c) ? a + c : 0, 0)}
✨   Longest streak: ${longestStreak}

✅   ${completedGames.length} games played
🏆   ${wonGames.length} games won (${((completedGames.length > 0) ? 100 * wonGames.length / completedGames.length : 0).toFixed(2)}%):
  `;

  // Guess chart div

  let guessChartDiv = document.createElement('div');

  let maxGameCount = Math.max(...Object.values(wonGamesByGuessCount));

  for (let [guessCount, gameCount] of Object.entries(wonGamesByGuessCount)) {
    let barLength;
    if (maxGameCount === 0) {
      barLength = 0;
    } else {
      barLength = 100 * gameCount / maxGameCount;
    }

    let rowDiv = document.createElement('div');
    rowDiv.classList.add('chart-row');

    let barText = document.createElement('div');
    barText.innerText = `${guessCount} guesses: `;
    barText.classList.add('chart-text');
    rowDiv.appendChild(barText);

    let barContainerDiv = document.createElement('div');
    barContainerDiv.classList.add('chart-bar-container');

    let barDiv = document.createElement('div');
    barDiv.classList.add('chart-bar');
    barDiv.style.width = `${barLength}%`;

    if (barLength > 0) {
      let gameCountSpan = document.createElement('span');
      gameCountSpan.innerText = gameCount;
      barDiv.appendChild(gameCountSpan);

      let gameCountPercentageSpan = document.createElement('span');
      gameCountPercentageSpan.innerText = ` ${(100 * gameCount / wonGames.length).toFixed(0)}%`;
      gameCountPercentageSpan.classList.add('bold');
      barDiv.appendChild(gameCountPercentageSpan);
    }

    barContainerDiv.appendChild(barDiv);
    rowDiv.appendChild(barContainerDiv);
    guessChartDiv.appendChild(rowDiv);
  }

  // Game meta div

  let gameMetaDiv = document.createElement('div');

  let completedGameDurations = completedGames.map((game) => (Date.parse(game.endTime) - Date.parse(game.startTime)) / 1000);

  gameMetaDiv.innerText = (completedGames.length === 0) ? '⏳ No games played yet' : `⏳ Your average game takes ${(completedGameDurations.reduce((a, c) => a + c) / completedGameDurations.length).toFixed(1)} seconds!`;

  return [
    statsHeaderDiv,
    gameSummaryDiv,
    guessChartDiv,
    gameMetaDiv
  ]
}


/* Game History */

function gameToDiv(gameJson) {
  let div = document.createElement('div');
  div.classList.add('history-card');

  let header = document.createElement('header');
  header.classList.add('history-header', 'row');

  let wordDiv = document.createElement('div');
  wordDiv.classList.add('history-word');

  for (let letter of gameJson['word']) {
    let span = document.createElement('span');
    span.classList.add('letter', 'history-letter', 'upper');
    switch (gameJson['status']){
      case 'won':
        span.classList.add('correct');
        span.innerText = letter;
        break;
      case 'lost':
        span.classList.add('incorrect');
        span.innerText = letter;
        break;
      case 'ongoing':
        span.classList.add('misplaced');
        span.innerText = '?';
        break;
    }
    wordDiv.appendChild(span);
  }

  let statusDiv = document.createElement('div');
  statusDiv.innerText = gameJson['status'];

  header.appendChild(wordDiv);
  header.appendChild(statusDiv);

  let gameDataDiv = document.createElement('div');

  let endTimeText = (gameJson['endTime'] !== undefined) ? `
  End time: ${gameJson['endTime']}` : '';

  console.log(gameJson);

  gameDataDiv.innerText = `
  ${settingsToText(gameJson['settings'])}

  ${gameJson['guesses'].length} guesses

  Start time: ${gameJson['startTime']}
  ${endTimeText}
  `

  div.appendChild(header);
  div.appendChild(gameDataDiv);

  return div
}


function settingsToText(settingsJson) {
  let languageText = {
    'en': '🇬🇧',
    'no': '🇧🇻'
  }[settingsJson['language']];
  let wordLengthText = `${settingsJson['wordLength']} letters`
  let difficultyText = `${settingsJson['wordDifficulty']}`
  let nAllowedGuessesText = `${settingsJson['nAllowedGuesses']} guesses`

  return `${languageText} ${wordLengthText}, ${difficultyText}, ${nAllowedGuessesText}`
}


/* Back button */

function registerBackButton(dialog, backDestinationFunction=showMenu) {
  let backLink = dialog.querySelector('.back-link');
  backLink.onclick = () => {
    dialog.close();
    backDestinationFunction();
  }
}
