'use strict'


if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("/serviceWorker.js")
      .then((reg) => {
        reg.addEventListener('updatefound', () => {
          const installingWorker = reg.installing;
          const waitingWorker = reg.waiting;
          const activeWorker = reg.active;
          console.log('New service worker is being installed:', installingWorker);
          console.log('Waiting service worker', waitingWorker);
          console.log('Active service worker', activeWorker);

          installingWorker.addEventListener('statechange', () => {
            console.log('New worker state has changed:', installingWorker.state);

            if (installingWorker.state === 'installed') {
              caches.keys().then((cacheNames) => {
                if (cacheNames.length === 1) {
                  console.log('You are on version', cacheNames);
                  const versionText = document.querySelector('#version-text');
                  versionText.innerText = cacheNames[0].replace('wordmoose-', '');
                } else if (cacheNames.length > 1) {
                  console.log('Multiple versions cached, please check caches settings menu.');
                } else {
                  console.log('No versions cached');
                }
              });
            }
          });
        });
        console.log("Service worker registered");

        const updateLink = document.querySelector('#update-link');
        updateLink.onclick = () => {
          updateLink.innerText = 'Updating...';
          reg.update().then(() => {
            localStorage.setItem('toOpen', 'menu');
            window.location.reload(true);
          });
        };

      })
      .catch((error) => {
        console.log('Service worker registration failed:', error);
      });
  });
} else {
  console.log('Service workers are not supported.');
}
