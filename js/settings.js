'use strict'


const DEFAULTSETTINGS = {
  language: 'en',
  wordLength: 5,
  wordDifficulty: 'easy',
  percentileWordLengthFilter: 5,
  percentileWordLengthFilterGuess: 100,
  nAllowedGuesses: 6
};

const WORDDIFFICULTIES = {
  supereasy: 1,
  easy: 5,
  medium: 20,
  hard: 50
};


function getSettings() {
  requestPersistentStorage();

  if (!localStorage.getItem('settings')) {
    localStorage.setItem('settings', JSON.stringify({}));
  }

  let settings = JSON.parse(localStorage.getItem('settings'));
  let settingsWithDefaultFill = Object.assign(DEFAULTSETTINGS, settings);

  localStorage.setItem('settings', JSON.stringify(settingsWithDefaultFill));

  return settingsWithDefaultFill
}


function saveSettings() {
  let previouslySavedSettings = getSettings();

  let activeSettings = {
    language: document.querySelector("#language-select").value,
    wordLength: document.querySelector("#word-length-select").value,
    wordDifficulty: document.querySelector('#word-difficulty-select').value,
    percentileWordLengthFilter: WORDDIFFICULTIES[document.querySelector('#word-difficulty-select').value]
  };

  let settings = Object.assign(previouslySavedSettings, activeSettings);
  
  localStorage.setItem('settings', JSON.stringify(settings));
}
